/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef UGB_SDL_H_
#define UGB_SDL_H_
    #include <SDL2/SDL.h>
    #include <stdint.h>
    #include "ugb/lcd.h"
    #include "gb.h"
    
    struct sdl_resources
    {
        SDL_Window* win;
        SDL_Renderer* ren;
        SDL_Texture* lcd;
    };
    int init_sdl(
        struct sdl_resources* res,
        unsigned w,
        unsigned h,
        const char *title
    );
    void quit_sdl(struct sdl_resources* res);
    void update_window(struct sdl_resources* res, struct gb_resources* gb);
#endif
