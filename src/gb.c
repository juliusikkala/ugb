/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "gb.h"
#include "io.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
static void save_callback(
    struct gb_resources* res,
    const uint8_t *ram,
    size_t ram_sz
){
    write_file(res->ram_path, ram, ram_sz);
}
static char * generate_save_path(const char *rom_path, const char *ram_path)
{
    char *path=NULL;
    if(ram_path!=NULL)
    {
        path=malloc(strlen(ram_path)+1);
        strcpy(path, ram_path);
    }
    else
    {
        /*Find last dot*/
        const char *last_dot=strrchr(rom_path,'.');
        size_t len=last_dot-rom_path+1;
        path=malloc(len+4);
        sprintf(path, "%.*ssav", (int)len, rom_path);
    }
    return path;
}
unsigned init_gb(
    struct gb_resources* res,
    const char *rom_path,
    const char *ram_path
){
    size_t rom_size=0;
    uint8_t *rom_data=NULL;
    size_t ram_size=0;
    uint8_t *ram_data=NULL;
    if(rom_path!=NULL)
    {
        rom_data=read_file(rom_path, &rom_size);
        if(rom_data==NULL)
        {
            return 1;
        }
    }
    else
    {
        return 1;
    }
    res->ram_path=generate_save_path(rom_path, ram_path);
    ram_data=read_file(res->ram_path, &ram_size);
    if(ram_data==NULL&&ram_path!=NULL)
    {/*A path was defined but it could not be loaded.*/
        free(rom_data);
        return 2;
    }
    res->cart=ugb_create_cartridge(rom_data, rom_size, ram_data, ram_size);
    free(rom_data);
    if(ram_data!=NULL)
    {
        free(ram_data);
    }
    if(!ugb_cartridge_ok(&res->cart))
    {
        return 3;
    }
    res->lcd=ugb_create_lcd(ugb_default_palette);
    res->sys=ugb_create_system(&res->cart, &res->lcd);
    ugb_set_callbacks(&res->sys, res, (ugb_save_callback)save_callback);
    res->debug=ugb_create_debug(&res->sys);
    return 0;
}
void reset_gb(struct gb_resources* res)
{
    ugb_reset_system(&res->sys);
}
void quit_gb(struct gb_resources* res)
{
    ugb_free_debug(&res->debug);
    ugb_free_system(&res->sys);
    ugb_free_lcd(&res->lcd);
    ugb_free_cartridge(&res->cart);
    free(res->ram_path);
}
unsigned save_gb(struct gb_resources* res, const char *ram_path)
{
    return !write_file(
        ram_path,
        ugb_cartridge_get_ram(&res->cart),
        UGB_RAM_BANK_SIZE
    );
}
void frame_gb(struct gb_resources* res)
{
    ugb_step_frame(&res->sys);
}
unsigned debug_gb(struct gb_resources* res)
{
    res->debug.enabled=1;
    ugb_step_debug(&res->debug);
    return res->debug.enabled==0?1:0;
}
