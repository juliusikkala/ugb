/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "io.h"
#include <stdio.h>
#include <stdlib.h>

uint8_t * read_file(const char *path, size_t* sz)
{
    uint8_t *data=NULL;
    FILE *f=fopen(path, "rb");
    if(f==NULL)
    {
        return NULL;
    }
    fseek(f, 0, SEEK_END);
    *sz=ftell(f);
    fseek(f, 0, SEEK_SET);
    data=malloc(*sz);
    fread(data, 1, *sz, f);
    fclose(f);
    return data;
}

unsigned write_file(const char *path, const uint8_t *data, size_t sz)
{
    FILE *f=fopen(path, "w");
    if(f==NULL)
    {
        return 0;
    }
    fwrite(data, 1, sz, f);
    fclose(f);
    return 1;
}
