/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef UGB_GB_H_
#define UGB_GB_H_
    #include "ugb/cartridge.h"
    #include "ugb/lcd.h"
    #include "ugb/system.h"
    #include "ugb/debug.h"
    
    struct gb_resources
    {
        struct ugb_cartridge cart;
        struct ugb_lcd lcd;
        struct ugb_system sys;
        struct ugb_debug debug;
        char* ram_path;
    };
    /*if ram is NULL, it is initialized with zeroes. Returns 0 on success*/
    unsigned init_gb(
        struct gb_resources* res,
        const char *rom_path,
        const char *ram_path
    );
    void reset_gb(struct gb_resources* res);
    void quit_gb(struct gb_resources* res);
    /*Returns 1 if it failed to write the file*/
    unsigned save_gb(struct gb_resources* res, const char *ram_path);
    void frame_gb(struct gb_resources* res);
    /*returns 1 if the user wants to end the debug session.*/
    unsigned debug_gb(struct gb_resources* res);
#endif
