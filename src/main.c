#include <stdio.h>
#include <signal.h>
#include "main.h"
static sig_atomic_t received_sigint=0;
static void main_signal_handler(int signal)
{
    (void)signal;
    received_sigint=1;
}

unsigned init(struct state* s, int argc, char **argv)
{
    s->gb_loaded=0;
    s->debug_mode=0;
    if(argc>3||argc<2)
    {
        printf("Usage: %s game.gb [game.ram]\n", argv[0]);
        return 1;
    }
    
    if(init_sdl(&s->sdl, 160, 144, "µGB"))
    {
        printf("Failed to initialize SDL. Quitting.\n");
        return 2;
    }
    SDL_Init(SDL_INIT_TIMER);
    if(argc>1)
    {
        unsigned err=init_gb(&s->gb, argv[1], argc==3?argv[2]:NULL);
        switch(err)
        {
        case 0:
            s->gb_loaded=1;
            break;
        case 1:
            printf("%s: No such file or directory\n", argv[1]);
            goto fail;
        case 2:
            if(argc==3)
            {
                printf("%s: No such file or directory\n", argv[2]);
            }
            else
            {
                printf("There was an error loading the RAM file for the GB\n");
            }
            goto fail;
        case 3:
            printf("%s: ROM corrupted.\n", argv[1]);
            goto fail;
        default:
            printf("Unknown error %d.\n", err);
            goto fail;
        }
        s->gb_loaded=1;
    }
    /*Install signal handler*/
    signal(SIGINT, main_signal_handler);
    return 0;
fail:
    quit(s);
    return 3;
}
void quit(struct state* s)
{
    quit_sdl(&s->sdl);
    if(s->gb_loaded)
    {
        quit_gb(&s->gb);
    }
}
unsigned handle_input(struct state* s)
{
    SDL_Event e;
    while(SDL_PollEvent(&e))
    {
        unsigned button=0;
        unsigned valid=1;
        switch(e.type)
        {
        case SDL_KEYDOWN:
        case SDL_KEYUP:
            switch(e.key.keysym.sym)
            {
            case SDLK_RIGHT:
                button=UGB_BUTTON_RIGHT;
                break;
            case SDLK_LEFT:
                button=UGB_BUTTON_LEFT;
                break;
            case SDLK_UP:
                button=UGB_BUTTON_UP;
                break;
            case SDLK_DOWN:
                button=UGB_BUTTON_DOWN;
                break;
            case SDLK_z:
                button=UGB_BUTTON_A;
                break;
            case SDLK_x:
                button=UGB_BUTTON_B;
                break;
            case SDLK_BACKSPACE:
                button=UGB_BUTTON_SELECT;
                break;
            case SDLK_RETURN:
                button=UGB_BUTTON_START;
                break;
            case SDLK_d:
                if(e.type==SDL_KEYDOWN)
                {
                    s->debug_mode=1;
                }
                valid=0;
                break;
            default:
                valid=0;
                break;
            }
            if(valid&&s->gb_loaded)
            {
                ugb_pad_event(
                    &s->gb.sys,
                    button,
                    e.type==SDL_KEYDOWN?UGB_BUTTON_PRESS:UGB_BUTTON_RELEASE
                );
            }
            break;
        
        case SDL_QUIT:
            return 1;
        default:
            break;
        }
    }
    return 0;
}
void main_loop(struct state* s)
{
    uint32_t frame_begin=0, frame_end=0, frame_time=0;
    frame_begin=SDL_GetTicks();
    while(!received_sigint)
    {
        frame_begin=SDL_GetTicks();
        if(s->gb_loaded)
        {
            if(s->debug_mode)
            {
                if(debug_gb(&s->gb))
                {
                    s->debug_mode=0;
                }
            }
            else
            {
                frame_gb(&s->gb);
            }
            
            update_window(&s->sdl, &s->gb);
        }
        frame_end=SDL_GetTicks();
        frame_time=frame_end-frame_begin;
        if(frame_time<UGB_FRAME_MS)
        {
            SDL_Delay(UGB_FRAME_MS-frame_time);
        }
        if(handle_input(s))
        {
            break;
        }
    }
}
int main(int argc, char **argv)
{
    struct state s;
    if(init(&s, argc, argv))
    {
        return 1;
    }
    main_loop(&s);
    quit(&s);
    return 0;
}
