/*
The MIT License (MIT)

Copyright (c) 2015 Julius Ikkala

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "sdl.h"
int init_sdl(
    struct sdl_resources* res,
    unsigned w,
    unsigned h,
    const char *title
){
    res->win=NULL;
    res->ren=NULL;
    res->lcd=NULL;
    if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_EVENTS|SDL_INIT_TIMER))
    {
        fprintf(stderr, "SDL_Init failed: %s\n", SDL_GetError());
        return 1;
    }
    res->win=SDL_CreateWindow(
        title,
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        w, h,
        0
    );
    if(res->win==NULL)
    {
        fprintf(stderr, "SDL_CreateWindow failed: %s\n", SDL_GetError());
        quit_sdl(res);
        return 2;
    }
    res->ren=SDL_CreateRenderer(res->win, -1, 0);
    if(res->ren==NULL)
    {
        fprintf(stderr, "SDL_CreateRenderer failed: %s\n", SDL_GetError());
        quit_sdl(res);
        return 3;
    }
    res->lcd=SDL_CreateTexture(
        res->ren,
        SDL_PIXELFORMAT_RGB24,
        SDL_TEXTUREACCESS_STREAMING,
        UGB_DISPLAY_WIDTH,
        UGB_DISPLAY_HEIGHT
    );
    if(res->lcd==NULL)
    {
        fprintf(stderr, "SDL_CreateTexture failed: %s\n", SDL_GetError());
        quit_sdl(res);
        return 4;
    }
    return 0;
}
void quit_sdl(struct sdl_resources* res)
{
    if(res->win!=NULL)
    {
        SDL_DestroyWindow(res->win);
        res->win=NULL;
    }
    if(res->ren!=NULL)
    {
        SDL_DestroyRenderer(res->ren);
        res->ren=NULL;
    }
    if(res->lcd!=NULL)
    {
        SDL_DestroyTexture(res->lcd);
        res->lcd=NULL;
    }
    SDL_Quit();
}
void update_window(struct sdl_resources* res, struct gb_resources* gb)
{
    SDL_UpdateTexture(
        res->lcd,
        NULL,
        gb->lcd.display,
        sizeof(struct ugb_color)*UGB_DISPLAY_WIDTH
    );
    SDL_RenderCopy(res->ren, res->lcd, NULL, NULL);
    SDL_RenderPresent(res->ren);
}
